// List Dependencies
var express = require('express');
var router = express.Router();

// Add DB & Model Dependencies
var mongoose = require('mongoose');
var Business = require('../models/business');

// Add Dependencies for file upload
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');

// Interpret GET /businesses - show business listing
router.get('/businesses', function(req, res, next) {

    // Retrieve all businesses using the Business model
    // Returns either an error or list of businesses
    try {
        Business.find(function(err, businesses) {
            if (err) {
                // if we have an error throw it!
                throw err;
            }
            else {
                if(businesses.length > 0) {
                    // No error, show the views/businesses.jade and pass the query results to that view
                    res.render('businesses', { businesses: businesses, title: 'List of Businesses' });
                }
                else {
                    res.render('businesses', { businesses: businesses, title: 'No Results Found' });
                }
            }
        });
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'Couldn\'t find businesses!', error: err });
    }

});

// Interpret GET /businesses/edit/:id - Show single business edit form
router.get('/businesses/edit/:id', function(req, res, next) {

    // Store the ID from the URL in a variable
    var id = req.params.id;

    // Use the Business model to look up the business with this id
    try {
        Business.findById(id, function(err, business) {
            if (err) {
                // if we have an error throw it!
                throw err;
            }
            else {
                res.render('edit', { business: business, title: 'Edit Business' });
            }
        });
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'Couldn\'t find business!', error: err });
    }

});

// Interpret POST /businesses/edit/:id - Update Business
router.post('/businesses/edit/:id', function(req, res, next) {

    // Create business variable to be used for the JSON object
    var business = {};

    // Use the formidable package to precess the file upload
    var form = new formidable.IncomingForm;

    // interpret form values, including the uploaded file
    try {
        form.parse(req, function(err, fields, files) {
            if (err) {
                throw err;
            }
            else {
                // If the user didn't choose a new image, use the old one.
                var logoPath;
                if (files.logoPath.name == '') {
                    logoPath = fields.previousLogo;
                }
                else {
                    logoPath = files.logoPath.name;
                }

                // Create the business JSON object to be used to update the Business later
                business = {
                    _id: fields.id,
                    name: fields.name,
                    description: fields.description,
                    logoPath: logoPath,
                    website: fields.website,
                    twitter: fields.twitter,
                    facebook: fields.facebook,
                    phoneNumber: fields.phoneNumber,
                    address: fields.address,
                    contactName: fields.contactName
                };
            }
        });
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'There was an error with the information entered!', error: err });
    }

    // Copy the file from the temp location to public/images
    try {
        form.on('end', function(fields, files) {
            // Check if the user uploaded a file, if they did, upload it!
            if(this.openedFiles[0].name != '') {
                // Get file name and path and set final directory
                var temp_path = this.openedFiles[0].path;
                var file_name = this.openedFiles[0].name;
                var new_path = 'public/images/businesses/';

                // Copy to Images Folder
                fs.copy(temp_path, new_path + file_name, function(err) {
                    if (err) {
                        throw err;
                        res.end('Image Uploaded Error.');
                    }
                    else {
                        // Use the Business model to update a business
                        Business.update({ _id: business._id }, business,
                            function(err, business) {
                                if (err) {
                                    throw err;
                                }
                                else {
                                    console.log('Business saved ' + business);
                                    res.render('edited', { business: business, title: 'Business Successfully Edited' });
                                }
                            }
                        );
                    }
                });
            }
            // If a user didn't upload a file, just update the data
            else {
                // Use the Business model to update a business
                Business.update({ _id: business._id }, business,
                    function(err, business) {
                        if (err) {
                            throw err;
                        }
                        else {
                            console.log('Business saved ' + business);
                            res.render('edited', { business: business, title: 'Business Successfully Edited' });
                        }
                    }
                );
            }
        });
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'There was an error saving the business!', error: err });
    }

});

// Interpret GET /businesses/add - show business input form
router.get('/businesses/add', function(req, res, next) {
    res.render('add', { title: 'Add a Business' });
});

// Interpret POST /business/add - save new business
router.post('/businesses/add', function(req, res, next) {

    // Create business variable to be used for the JSON object
    var business = {};

    // Use the formidable package to precess the file upload
    var form = new formidable.IncomingForm;

    // interpret form values, including the uploaded file
    try {
        form.parse(req, function(err, fields, files) {
            if (err) {
                throw err;
            }
            else {
                // If the user didn't choose an image, use a default one.
                var logoPath;
                if (files.logoPath.name == '') {
                    logoPath = 'default.jpg';
                }
                else {
                    logoPath = files.logoPath.name;
                }

                // Create the business JSON object to be used to update the Business later
                business = {
                    name: fields.name,
                    description: fields.description,
                    logoPath: logoPath,
                    website: fields.website,
                    twitter: fields.twitter,
                    facebook: fields.facebook,
                    phoneNumber: fields.phoneNumber,
                    address: fields.address,
                    contactName: fields.contactName
                };
            }
        });
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'There was an error with the information entered!', error: err });
    }



    // Copy the file from the temp location to public/images
    try {
        form.on('end', function(fields, files) {
            // Check if the user uploaded a file, if they did, upload it!
            if(this.openedFiles[0].name != '') {
                // Get file name and path and set final directory
                var temp_path = this.openedFiles[0].path;
                var file_name = this.openedFiles[0].name;
                var new_path = 'public/images/businesses/';

                // Copy to Images Folder
                fs.copy(temp_path, new_path + file_name, function(err) {
                    if (err) {
                        throw err;
                        res.end('Image Uploaded Error.');
                    }
                    else {
                        // Use the Business model to insert a new product
                        Business.create(business, function(err, business) {
                            if (err) {
                                throw err;
                            }
                            else {
                                console.log('Business saved ' + business);
                                res.render('added', { business: business, title: 'Business Saved Successfully' });
                            }
                        });
                    }
                });
            }
            // If a user didn't upload a new file, just update the data
            else {
                // Use the Business model to insert a new product
                Business.create(business, function(err, business) {
                    if (err) {
                        throw err;
                    }
                    else {
                        console.log('Business saved ' + business);
                        res.render('added', { business: business, title: 'Business Saved Successfully' });
                    }
                });
            }
        });
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'There was an error saving the business!', error: err });
    }

});

// Interpret GET /businesses/delete/id - delete business
router.get('/businesses/delete/:id', function(req, res, next) {

    // Store the ID from the URL in a variable
    var id = req.params.id;

    // Use the Business model to look up the business with this id
    try {
        Business.findById(id, function(err, business) {
            if (err) {
                throw err;
            }
            else {
                res.render('deleteConfirm', { business: business, title: 'Confirm Delete Business' });
            }
        });
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'There was an error finding that business!', error: err });
    }

});

// Interpret GET /businesses/delete/id - delete business
router.get('/businesses/delete/:id/yes', function(req, res, next) {

    // Store the ID from the URL into a variable
    var id = req.params.id;

    // Use our Business model to delete
    try {
        Business.remove(
            { _id: id },
            function(err, business) {
                if (err) {
                    throw err;
                }
                else {
                    console.log('Business removed ' + business);
                    res.render('deleted', { title: 'Business Successfully Deleted' })
                }
            }
        );
    }
    catch(err) {
        console.log(err);
        res.render('error', { title: 'Error!', message: 'There was an error deleting that business!', error: err });
    }

});

// API GET all businesses request handler
router.get('/api/businesses/', function(req, res, next) {

    // Retrieve all businesses using the Business model
    // Returns either an error or list of businesses
    Business.find(function(err, businesses) {
        if (err) {
            // if we have an error
            console.log(err);
            res.send(err);
        }
        else {
            // No error, send back businesses JSON
            res.send(businesses);
        }
    });

});

// API GET business by ID or NAME request handler
router.get('/api/businesses/:query', function(req, res, next) {

    // Retrieve a business using the Business model
    // Returns either an error or the business listing from the ID
    Business.find({ _id: req.params.query }, function(err, business) {
        if (err) {
            // if we have an error then it's not an ID
            // Find the businesses by name
            Business.find({ name: req.params.query }, function(err, businesses) {
                if (err) {
                    // if we have an error
                    console.log(err);
                    res.send(err);
                }
                else {
                    // No error, send back business JSON
                    res.send(businesses);
                }
            });
        }
        else {
            // No error, send back business JSON
            res.send(business);
        }
    });

});

// Make the controller public
module.exports = router;