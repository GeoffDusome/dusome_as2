var express = require('express');
var router = express.Router();

// Use basic auth for admin authentication
var basicAuth = require('basic-auth-connect');

// Setup basic auth
var adminAuth = basicAuth(function(user, pass, fn) {
    // Setup username and password for admin login.
   var result = (user === 'admin' && pass === 'admin');

   // If result returns a value, login!
   if (result) {
      fn(null, user);
   }
   // If result doesn't return a value, ask them to re-enter!
   else {
      fn(null);
   }
});

router.use('/admin', adminAuth);

/* GET home page. */
router.get('/admin', function(req, res, next) {
    res.render('admin', {title: 'COOL HIDDEN STUFF!'});
});

module.exports = router;