$(document).ready(function() {

    // Hide business information drop-down
    $('.business-listing').hide();

    // Function to show and hide image upload preview
    if($('#logoPath').length > 0) {
        $('.logoPreview').hide();
        $('#logoPath').change(function() {
            // readURL function, see below
            readURL(this);
            $('.logoPreview').fadeIn();
        });
    }

    // On click of a business title, expand it!
    $('.business-collapse').click(function( event ) {
        if ( $(event.target).is('div.business-collapse') ||  $(event.target).is('div.business-collapse h2') ) {
            $(this).next().slideToggle();
        }
    });

    // The readURL function gets the file from the system and displays it in the browser.
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.logoPreview > img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    // Show/Hide Error message more details
    $('.error-message-show').click(function( event ) {
        event.preventDefault();
        if ($('.error-message-show').html() == 'Show More...') {
            $('.error-message-show').html('Show Less...');
            $('.error-message pre').slideToggle();
        }
        else {
            $('.error-message-show').html('Show More...');
            $('.error-message pre').slideToggle();
        }
    });
});