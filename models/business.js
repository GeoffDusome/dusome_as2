// db link
var mongoose = require('mongoose');

// Define the product model (fields and data types)
var BusinessSchema = new mongoose.Schema({
    name: String,
    description: String,
    logoPath: String,
    website: String,
    twitter: String,
    facebook: String,
    phoneNumber: String,
    address: String,
    contactName: String
});

// Make the model public so other files can access it
module.exports = mongoose.model('Business', BusinessSchema);